import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationAfterSubmit } from '../../shared/plugins';
import { Task } from '../../shared/models/task.model';
import { EmployeeService } from '../../shared/services/api';
import {MessageService} from 'primeng/api';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  providers: [MessageService]
})
export class EditComponent implements OnInit {

  submitted = false;
  Task = new Task;
  TaskForm: FormGroup;
  showData;
  id;
  constructor(
    private fb: FormBuilder,
    private taskService: EmployeeService, 
    private isFormValid: ValidationAfterSubmit,
    private messageService: MessageService,
    public config: DynamicDialogConfig

     ) { 
      console.log(config.data)
    }

  ngOnInit(): void {
    this.initForm()
  }

  private initForm() {
    this.TaskForm = this.fb.group({
      Name: [null, [Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z ]{2,}")]],
      Description: [null, Validators.required],
      Url:[null, Validators.required],
      UserName: [null, Validators.required],
      Password: [null, Validators.required],
      WorkImg: [null],


    });
    if(this.config.data.id){
      this.showDataForm(this.config.data.id)
    }
  }
  get f() {
    return this.TaskForm.controls;
  }
  showDataForm(id) {
    this.taskService.getTaskById(id).subscribe(data => {
      this.showData = data
      this.TaskForm.patchValue({
        Name: this.showData.Name,
        Description: this.showData.Description,
        Url: this.showData.Url,
        UserName: this.showData.UserName,
        Password: this.showData.Password,
        WorkImg: this.showData.WorkImg
      });

    });
  }
  editTask() {
    this.Task = this.TaskForm.value;
    this.isFormValid.afterSubmit(this.TaskForm);
    this.submitted = true;

    if (this.TaskForm.valid == true) {
      this.taskService.updateTask(this.config.data.id,this.Task).subscribe(res => {
        // if (res.status == 200) {
          this.messageService.add({severity:'success',
          summary: 'Success', detail: 'Task Updated Successfully'});
          setTimeout(() =>{
            window.location.reload();
          }, 1000);
         
        // }
        // else {
          // this.sweetAlert.afterSubmit('false', 'Please Check Your Feild Again')
        
        // }
      })
    }
  }
}
