import { Injectable } from '@angular/core';
import { EndPoints } from './endpoints';
import { HttpClient } from "@angular/common/http";
import { Task} from "../../models";
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private URL = EndPoints.BASE_URL
  private allTasksApiUrl = EndPoints.ALL_TASKS_ENDPOINT;
  private newTaskApiUrl = EndPoints.NEW_TASK_ENDPOINT;
  private deleteTaskApiUrl = EndPoints.DELETE_ENDPOINT;
  private editTaskApiUrl = EndPoints.EDIT_TASK_ENDPOINT;
  private getByIdTaskUrl = EndPoints.GetById_TASK_ENDPOINT;
  

  
  list: Task[];
  constructor(private http: HttpClient, private router: Router) { }
 
  getallTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(this.allTasksApiUrl);
  }
 
  getTaskById(id: string): Observable<Task> {  
    return this.http.get<Task>(`${this.getByIdTaskUrl}/${id}`);  
  }  
  createTask(data: any): Observable<any> {
    return this.http.post(this.newTaskApiUrl, data);
  }
  deleteTask(id: string): Observable<any> {
    return this.http.delete(`${this.deleteTaskApiUrl}/${id}`);
  }
  updateTask(id: string, data:any): Observable<Task> {
    return this.http.put(`${this.editTaskApiUrl}/${id}`, data );
  }

 

}
